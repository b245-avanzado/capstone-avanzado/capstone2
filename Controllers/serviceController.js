const Service = require("../Models/servicesSchema.js");
const auth = require("../auth.js");

// CONTROLLERS

// This controller will add a service Start
module.exports.addService = (request, response) => {
	let input = request.body;

	let newService = new Service({
		name: input.name,
		description: input.description,
		price: input.price
	});

	const userData = auth.decode(request.headers.authorization);

	Service.findOne({name: input.name})
	.then(result => {
		if (userData.isAdmin) {

			if (result !== null) {
				return response.send("This service already exists.");
			} else {
				newService.save();
				return response.send(`You have successfully created a ${input.name}`);
			}	

		} else {
			return response.send("You do not have permission to create a Service.");
		}
	})
	.catch(error => {
		return response.send(error);
	})
}
// This controller will add a service End

// This controller will retrieve all services Start
module.exports.getAllServices = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if (!userData.isAdmin) {
		return response.send("You do not have access to this route.");
	} else {
		Service.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}
// This controller will retieve all services End

// This controller will retrieve a single service Start
module.exports.getService = (request, response) => {
	const idToBeRetrieved = request.params.serviceId;

	Service.findById(idToBeRetrieved)
	.then(result => {
		if (result == null) {
			return response.send("This service does not exist.");
		} else {
			return response.send(result);
		}
	})
	.catch(error => response.send(error));
}
// This controller will retrieve a single service End

// This controller will update service information Start
module.exports.updateServiceInfo = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const input = request.body;

	const serviceId = request.params.serviceId;

	const updatedServiceInfo = {
		name: input.name,
		description: input.description,
		price: input.price
	};

	if (!userData.isAdmin) {
		return response.send("You do not have permission to update Service Information!");
	} else {
		Service.findById(serviceId)
		.then(result => {
			if (result == null) {
				return response.send("Service Id is invalid, please input a valid one.");
			} else {
				Service.findByIdAndUpdate(serviceId, updatedServiceInfo, {new: true})
				.then(result => response.send(result))
				.catch(error => response.send(error));
			}
		})
		.catch(error => response.send(error));
	}
}
// This controller will update service information End

// This controller will archive a service Start
module.exports.archiveService = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const serviceId = request.body._id;

	Service.findById(serviceId)
	.then(result => {
		if (!userData.isAdmin) {
			return response.send("You do not have permission to archive a service");
		} else {
			if (result == null) {
				return response.send("This service does not exist!");
			} else if (!result.isActive) {
				return response.send("This service is already archived!");
			} else {
				Service.findByIdAndUpdate(serviceId, {isActive: false}, {new: true})
				.then(result => response.send(`You have successfully archived ${result.name}`))
				.catch(error => response.send(error));
			}
		}
	})
	.catch(error => response.send(error));
}
// This controller will archive a service End

// This controller will unarchive a service Start
module.exports.unarchiveService = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const serviceId = request.body.serviceId;

	Service.findById(serviceId)
	.then(result => {
		if (!userData.isAdmin) {
			return response.send("You do not have permission to unarchive a service");
		} else {
			if (result == null) {
				return response.send("This service does not exist!");
			} else if (result.isActive) {
				return response.send("This service is already active!");
			} else {
				Service.findByIdAndUpdate(serviceId, {isActive: true}, {new: true})
				.then(result => response.send(`You have successfully unarchived ${result.name}`))
				.catch(error => response.send(error));
			}
		}
	})
	.catch(error => response.send(error));
}
// This controller will unarchive a service End

// This controller will retrieve all active services Start
module.exports.getActiveServices = (request, response) => {
	Service.find({isActive: true})
	.then(result => {
		if (result == null) {
			return response.send("There are no services available yet.")
		} else {
			return response.send(result)
		}
	})
	.catch(error => response.send(error));
}
// This controller will retrieve all active services End

// This controller will retrieve all inactive services Start
module.exports.getInactiveServices = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if (!userData.isAdmin) {
		return response.send("You do not have permission to view archived services!")
	} else {
		Service.find({isActive: false})
		.then(result => {
			if (result == null) {
				return response.send("There are no archived services yet.")
			} else {
				return response.send(result)
			}
		})
		.catch(error => response.send(error));
	}
}
// This controller will retrieve all inactive services End

