const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Service = require("../Models/servicesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// CONTROLLERS

// This controller will create/register a user Start
module.exports.userRegistration = (request, response) => {
	const input = request.body;

	User.findOne({email:input.email})
	.then(result => {
		if (result !== null) {
			return response.send("The email is already taken! Please enter another one.");
		} else {
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				address: input.address,
				mobileNo: input.mobileNo
			})

			newUser.save()
			.then(save => {
				return response.send("You have successfully created an account.");
			})
			.catch(error => {
				return response.send(error);
			})
		}
	})
	.catch(error => {
		return response.send(error);
	})
}
// This controller will create/register a user End

// This controller will create an access token for a user Start
module.exports.userAuthentication = (request, response) => {
	let input = request.body;
	
	User.findOne({email:input.email})
	.then(result => {
		if (result === null) {
			return response.send("Email is not yet registered. Register first before logging in!")
		} else {

			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password);

			if (isPasswordCorrect) {
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send("Password is incorrect!");
			}
		}
	})
	.catch(error => {
		return response.send(error);
	})
}
// This controller will create an access token for a user End

// This controller will retrieve a user's details Start
module.exports.userProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(result => {
		if (result.isAdmin) {
			result = {
				firstName: result.firstName,
				lastName: result.lastName,
				email: result.email,
				isAdmin: result.isAdmin,
				mobileNo: result.mobileNo
			}
		} else {
			result = {
				firstName: result.firstName,
				lastName: result.lastName,
				email: result.email,
				address: result.address,
				isServiceProvider: result.isServiceProvider,
				mobileNo: result.mobileNo
			}
		}

		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}
// This controller will retrieve a user's details End

// This controller will avail a service Start
module.exports.availService = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const input = request.body;

	User.findById(userData._id)
	.then(user => {
		if (user == null) {
			return response.send("You are not registered. Please register first.");
		} else {
			if (userData.isAdmin) {
				return response.send("You can not avail a service.");
			} else {
				Service.findOne({name: input.serviceName})
				.then(service => {
					if (service == null) {
						return response.send("This service does not exist!")
					} else if (!service.isActive) {
						return response.send(`Sorry, ${service.name} is not available right now. Please choose another service.`)
					} else {

						service.clients.push({userId: userData._id, address: input.address, scheduledDate: input.scheduledDate});
						user.servicesAvailed.push({name: input.serviceName, scheduledDate: input.scheduledDate});

						service.save();
						user.save();

						return response.send(`${user.firstName}, you have successfully booked ${service.name}!`)
					}
				})
				.catch(error => response.send(error));
			}
		}
	})
	.catch(error => response.send(error));

}
// This controller will avail a service End

// This controller will make a user an admin Start
module.exports.updateAdmin = (request, response) => {
	const input = request.body;

	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(admin => {
		if (admin == null) {
			return response.send("You are not registered yet. Please register first.");
		} else {
			if (!userData.isAdmin) {
				return response.send("You do not have access to this route!");
			} else {
				User.findByIdAndUpdate(input.userId, {isAdmin: true}, {new: true})
				.then(user => response.send(`You have successfully made ${userData.name} an admin!`))
				.catch(error => response.send(error));
			}
		}
	})
	.catch(error => response.send(error));
}
// This controller will make a user an admin End

// This controller will retrieve user's pending services Start
module.exports.getPendingServices = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(user => {
		if (user == null) {
			return response.send("You are not yet registered. Please register first.")
		} else {
			if (userData.isAdmin) {
				return response.send("You have no services availed.");
			} else {
				let pendingServices = user.servicesAvailed.map(service => {
					if (service.status == "Pending") {
						return service;
					}
				})
				response.send(pendingServices);
			}
		}
	})
	.catch(error => response.send(error));
}
// This controller will retrieve user's pending services End

// This controller will retrieve all of user's services Start
module.exports.getAllServices = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(result => {
		if (result == null) {
			return response.send("You are not yet registered. Please register first.")
		} else {
			if (userData.isAdmin) {
				return response.send("You have no services availed.");
			} else {
				return response.send(result.servicesAvailed);
			}
		}
	})
	.catch(error => response.send(error));
}
// This controller will retrieve all of user's services End

// This controller will cancel a user's availed service Start
module.exports.cancelService = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	const availedServiceIndex = request.params.availedServiceIndex;

	User.findById(userData._id)
	.then(user => {
		if (user == null) {
			return response.send("You are not yet registered. Please register first.")
		} else {
			if (userData.isAdmin) {
				return response.send("You have no services availed.");
			} else {
				if (availedServiceIndex < user.servicesAvailed.length) {
					user.servicesAvailed[availedServiceIndex].status = "Cancelled";
					user.save();
					return response.send(`${user.servicesAvailed[availedServiceIndex].name} is now cancelled.`);
				} else {
					return response.send("This service does not exist.")
				}
				

			}
		}
	})
	.catch(error => response.send(error));
}
// This controller will cancel a user's availed service End