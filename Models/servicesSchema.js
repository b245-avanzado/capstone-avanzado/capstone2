const mongoose = require("mongoose");

const servicesSchema = new mongoose.Schema ({
	name: {
		type: String,
		required: [true, "Service name is required!"]
	},
	description: {
		type: String,
		required: [true, "Service description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Service price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	clients: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required."]
			},
			address: {
				type: String,
				required: [true, "User address is required."]
			},
			scheduledDate: {
				type: Date,
				required: [true, "Please enter date of schedule."]
			}
		}
	]
})

module.exports = mongoose.model("Service", servicesSchema);