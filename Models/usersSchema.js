const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Please input your first name."]
	},
	lastName: {
		type: String,
		required: [true, "Please input your last name."]
	},
	email: {
		type: String,
		required: [true, "Email address is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	address: {
		type: String,
		required: [true, "Address is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isServiceProvider: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Please input your contact number"]
	},
	servicesAvailed: [
		{
			name: {
				type: String,
				required: [true, "Service name is required!"]
			},
			scheduledDate: {
				type: Date,
				required: [true, "Please enter date of schedule."]
			},
			status: {
				type: String,
				default: "Pending"
			}
		}
	]
})


module.exports = mongoose.model("User", usersSchema);