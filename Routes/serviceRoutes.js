const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const serviceController = require("../Controllers/serviceController.js");

// ROUTES WITHOUT PARAMS

// This route will create/register a user
router.post("/addService", auth.verify, serviceController.addService);

// This route will retrieve all services
router.get("/allServices", auth.verify, serviceController.getAllServices);

// This route will retrieve all active services
router.get("/activeServices", serviceController.getActiveServices);

// This route will retrieve all inactive services
router.get("/inactiveServices", auth.verify, serviceController.getInactiveServices);

// This route will archive a service
router.put("/archiveService", auth.verify, serviceController.archiveService);

// This route will archive a service
router.put("/unarchiveService", auth.verify, serviceController.unarchiveService);



// ROUTES WITH PARAMS

// This route will retrieve a single service
router.get("/:serviceId", serviceController.getService);

// This route will update service information
router.put("/update/:serviceId", auth.verify, serviceController.updateServiceInfo);




module.exports = router;