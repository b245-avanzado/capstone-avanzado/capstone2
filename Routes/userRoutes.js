const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController");

// ROUTES WITHOUT PARAMS

// This route will create/register a user
router.post("/register", userController.userRegistration);

// This route will create an access token for a user
router.post("/login", userController.userAuthentication);

// This route will retrieve a user's details Start
router.get("/profile", auth.verify, userController.userProfile);

// This route will avail a service
router.post("/availService", auth.verify, userController.availService);

// This route will make a user an admin
router.put("/updateAdmin", auth.verify, userController.updateAdmin);

// This route will retrieve user's pending services
router.get("/pendingServices", auth.verify, userController.getPendingServices);

// This route will retrieve all of user's services
router.get("/allServices", auth.verify, userController.getAllServices);

// ROUTES WITH PARAMS

// This route will cancel an availed service
router.put("/cancelService/:availedServiceIndex", auth.verify, userController.cancelService);


module.exports = router;