const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./Routes/userRoutes.js")
const serviceRoutes = require("./Routes/serviceRoutes.js")

const port = 4000;

const app = express();

	mongoose.set('strictQuery', true);
	// MongoDB Connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-avanzado.ixvkr4o.mongodb.net/capstone2_Avanzado?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


	let db = mongoose.connection;


	// For error handling
	db.on("error", console.error.bind(console, "Connection Error!"));
	// For validation of the connection
	db.once("open", () => {console.log("We are now connected to the cloud!")});

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());

// Routing
app.use("/user", userRoutes);
app.use("/service", serviceRoutes);



app.listen(port, () => console.log(`Server is running at port ${port}!`))